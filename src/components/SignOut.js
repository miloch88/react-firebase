import React from 'react';
import './SignOut.css';

import { withFirebase } from "./Firebase/index";

const SignOutButton = ({firebase}) =>(
    <button className="signout" type="button" onClick={firebase.doSignOut}>
    Sign Out
  </button>
);

export default withFirebase(SignOutButton);

